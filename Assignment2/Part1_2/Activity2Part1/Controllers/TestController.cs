﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Activity2Part1.Models;

namespace Activity2Part1.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            List<UserModel> user = new List<UserModel>();

            UserModel user1 = new UserModel("Zach","Z@gmail.com", "999-999-9999");
            UserModel user2 = new UserModel("Bob", "B@gmail.com", "999-999-9999");

            user.Add(user1);
            user.Add(user2);

            

            return View("Test", user);

        }

        

    }
}