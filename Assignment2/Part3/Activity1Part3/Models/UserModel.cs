﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Activity1Part3.Models
{
    public class UserModel
    {
        [Required(ErrorMessage = "Username is required")]
        [DisplayName("User Name")]
        [StringLength(20, MinimumLength = 4,ErrorMessage = "Minimum 4 characters")]
        [DefaultValue("")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [DisplayName("Password")]
        [StringLength(20, MinimumLength = 4, ErrorMessage = "Minimum 4 characters")]
        [DefaultValue("")]
        public string Password { get; set; }
    }
}