﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Activity4.Services.Data;
using Activity4.Models;


namespace Activity4.Services.Business
{
    public class SecurityService
    {
        public bool Authenticate(UserModel user)
        {
            SecurityDAO service = new SecurityDAO();
            return service.FindByUser(user);
        }
    }
}