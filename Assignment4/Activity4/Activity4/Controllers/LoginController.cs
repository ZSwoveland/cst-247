﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Activity4.Models;
using Activity4.Services.Business;
using Activity4.Services.Utility;
using NLog;

namespace Activity4.Controllers
{
    public class LoginController : Controller
    {
        //private static Logger logger = LogManager.GetLogger("myAppLoggerRules");
        private static MyLogger1 logger = MyLogger1.Instance;
        // GET: Login

        [HttpGet]
        public ActionResult Index()
        {
            //Validate the Form POST
            if (!ModelState.IsValid)
                return View("Login");

            return View("Login");
        }



        [HttpPost]

        public ActionResult Login(UserModel model)
        {
            try
            {
                SecurityService service = new SecurityService();
                bool Bool = service.Authenticate(model);

                logger.Info("Entering LoginController.LoginIN");
                logger.Info("Parameter Username: " + model.Username + " Password: " + model.Password);

                if (Bool == true)
                {
                    logger.Info("Exit LoginController.LoginIN with Login Passed");
                    return View("LoginPassed");
                }
                else
                {
                    logger.Info("Exit LoginController.LoginIN with Login Failed");
                    return View("LoginFailed");
                }
            }
            catch (Exception e)
            {
                logger.Error("Exception LoginController.LogIN");
                return View("LoginFailed");
            }


        }

    }
}