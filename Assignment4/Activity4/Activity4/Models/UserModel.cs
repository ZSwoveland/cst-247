﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Runtime.Serialization;

namespace Activity4.Models
{
    [DataContract]
    public class UserModel
    {
        [Required(ErrorMessage = "Username is required")]
        [DisplayName("User Name")]
        [StringLength(20, MinimumLength = 4, ErrorMessage = "Minimum 4 characters")]
        [DefaultValue("")]
        [DataMember]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [DisplayName("Password")]
        [StringLength(20, MinimumLength = 4, ErrorMessage = "Minimum 4 characters")]
        [DefaultValue("")]
        [DataMember]
        public string Password { get; set; }
    }
}