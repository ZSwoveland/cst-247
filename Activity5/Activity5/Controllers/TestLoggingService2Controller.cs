﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Activity5.Services.Utility;
using Unity;

namespace Activity5.Controllers
{
    public class TestLoggingService2Controller : Controller
    {
        [Dependency]
        public ILogger logger { get; set; }
        // GET: TestLoggingService2
        public string Index()
        {
            return logger.Info("Hello World 2 2");
        }
    }
}