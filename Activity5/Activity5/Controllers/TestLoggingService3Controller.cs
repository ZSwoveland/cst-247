﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Activity5.Services.Business;
using Activity5.Services.Utility;
namespace Activity5.Controllers
{
    public class TestLoggingService3Controller : Controller
    {

        private readonly ILogger logger;
        private readonly ITestService service;
        // GET: TestLoggingService3
        public string Index()
        {
            return service.TestLogger();
        }
    }
}