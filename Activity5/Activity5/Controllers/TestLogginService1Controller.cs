﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Activity5.Services.Utility;

namespace Activity5.Controllers
{
    public class TestLogginService1Controller : Controller
    {
        private static ILogger logger =  MyLogger1.Instance;
        // GET: TestLogginService1
        public string Index()
        {
            return logger.Info("Hello World");
        }
    }
}