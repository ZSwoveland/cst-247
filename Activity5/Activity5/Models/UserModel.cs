﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Activity5.Models
{
    public class UserModel
    {
        //[Required(ErrorMessage = "Username is required")]
        //[DisplayName("User Name")]
        //[StringLength(20, MinimumLength = 4, ErrorMessage = "Minimum 4 characters")]
        //[DefaultValue("")]
        //[DataMember]
        public string Username { get; set; }

        //[Required(ErrorMessage = "Password is required")]
        //[DisplayName("Password")]
        //[StringLength(20, MinimumLength = 4, ErrorMessage = "Minimum 4 characters")]
        //[DefaultValue("")]
        //[DataMember]
        public string Password { get; set; }
    }
}