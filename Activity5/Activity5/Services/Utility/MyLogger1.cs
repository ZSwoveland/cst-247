﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLog;

namespace Activity5.Services.Utility
{
    public class MyLogger1:ILogger
    {
        private static Logger logger = LogManager.GetLogger("myAppLoggerRules");
        private static readonly MyLogger1 instance = new MyLogger1();

        static MyLogger1()
        {

        }
        private MyLogger1()
        {

        }
        public static MyLogger1 Instance
        {
            get
            {

                return instance;
            }
        }



        public void Debug(string message)
        {

        }

        public void Error(string message)
        {
            logger.Error(message);
        }

        public string Info(string message)
        {
            return message;
        }

        public void Warning(string message)
        {

        }
    }
}