﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLog;

namespace Activity5.Services.Utility
{
    public class MyLogger2
    {

        private  Logger logger = LogManager.GetLogger("myAppLoggerRules");
        private static readonly MyLogger2 instance = new MyLogger2();

        static MyLogger2()
        {

        }
     
        



        public void Debug(string message)
        {

        }

        public void Error(string message)
        {
            logger.Error(message);
        }

        public void Info(string message)
        {
            logger.Info(message);
        }

        public void Warning(string message)
        {

        }
    }
}