﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Activity5.Services.Utility
{
    public interface ILogger
    {
       
        void Debug(string message);
        string Info(string message);
        void Warning(string message);

        void Error(string message);
    }
}
