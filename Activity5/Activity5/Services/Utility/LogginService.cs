﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Activity5.Services.Utility
{
    public class LogginService : ILogginService
    {
        private static readonly LogginService instance = new LogginService();
        static LogginService()
        {

        }
        private LogginService()
        {

        }
        public static LogginService Instance
        {
            get
            {

                return instance;
            }
        }
        public string Debug(string message)
        {
            return message;
        }

        public string Error(string message)
        {
            return message;
        }

        public string Info(string message)
        {
            return message;
        }

        public string Warning(string message)
        {
            return message;
        }
    }
}