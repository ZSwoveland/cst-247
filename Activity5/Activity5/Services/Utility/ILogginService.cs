﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Activity5.Services.Utility
{
    interface ILogginService
    {
        string Debug(string message);
        string Info(string message);
        string Warning(string message);

        string Error(string message);
    }
}
