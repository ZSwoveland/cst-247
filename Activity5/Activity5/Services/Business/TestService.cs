﻿using Activity5.Services.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Unity;

namespace Activity5.Services.Business
{
    public class TestService : ITestService
    {
        private static ILogger logger1 = MyLogger1.Instance;

        [InjectionMethod]
        public void Initialize(ILogger logger)
        {
            logger1 = logger;
        }

        public string TestLogger()
        {
           return logger1.Info("Test Loggin in TestService.TestLogger() invoked");
        }
    }
}