﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Activity5.Models;
using Activity5.Services.Data;

namespace Activity5.Services.Business
{
    public class SecurityService
    {
        public bool Authenticate(UserModel user)
        {
            SecurityDAO service = new SecurityDAO();
            return service.FindByUser(user);
        }
    }
}