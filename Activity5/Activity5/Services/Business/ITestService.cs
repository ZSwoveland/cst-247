﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Activity5.Services.Utility;


namespace Activity5.Services.Business
{
    interface ITestService 
    {
        
        public void Initialize(ILogger logger);
        

        public string TestLogger();
        

    }
}
