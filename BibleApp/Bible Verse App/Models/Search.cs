﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Bible_Verse_App.Models
{
    public class Search
    {
        [Required]
        public string SearchedBook { get; set; }

        public string SearchChap { get; set; }
        //public Search(string word)
        //{
        //    SearchedBook = word;
        //}

       public Search()
        {

        }
    }
}