﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bible_Verse_App.Models;

namespace Bible_Verse_App.Services
{
    public class SecurityService
    {
        public bool Authenticate(UserModel user)
        {
            SecurityDAO service = new SecurityDAO();
            return service.FindByUser(user);
        }
    }
}