﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Mvc;
using Bible_Verse_App.Models;
using Bible_Verse_App.Services;

namespace Bible_Verse_App.Controllers
{
    public class BibleVerseEntryController : Controller
    {
        private static MyLogger1 logger = MyLogger1.Instance;
        // GET: BibleVerseEntry
        [HttpPost]
        public ActionResult Index(Search search)
        {
            ViewBag.Message = FileRead(search.SearchedBook, search.SearchChap);
            return View("BibleVerseEntry");
        }

        //Reading files from the text file
       
        public string FileRead(string s, string ch)
        {
            logger.Info("Search Parameters Book: " + s + " Chapter: " + ch);

            string lines;
            try
            {
                using (StreamReader sr = new StreamReader("C://Users//zachs//source//repos//Bible Verse App//Bible Verse App//BibleVerses//"+s+"//"+s+ch+".txt"))
                {


                    lines = sr.ReadToEnd();
                    return lines;




                }

            }
            catch
            {
                return "Something went wrong";
            }


        }

    }
}