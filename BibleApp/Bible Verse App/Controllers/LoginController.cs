﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bible_Verse_App.Models;
using Bible_Verse_App.Services;

namespace Bible_Verse_App.Controllers
{
    public class LoginController : Controller
    {
        private static MyLogger1 logger = MyLogger1.Instance;
        // GET: Login
        public ActionResult Index()
        {
            return View("Login");
        }

        [HttpPost]
        public ActionResult Login(UserModel model)
        {
            logger.Info("Entering LoginController Loginin");
            SecurityService service = new SecurityService();
            bool Bool = service.Authenticate(model);
            logger.Info("Parameter Username: " + model.Username + " Password: " + model.Password);
            if (Bool == true)
            {
                logger.Info("Going into BibleVerseSearchPage");
                return View("BibleVerseSearch");
            }
            else
            {
                logger.Info("Login in failed");
                return View("LoginFailed");
            }
        }

    }
}