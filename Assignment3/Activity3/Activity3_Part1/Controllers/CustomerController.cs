﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Activity3_Part1.Models;

namespace Activity3_Part1.Controllers
{
    public class CustomerController : Controller
    {
        public List<CustomerModel> CM = new List<CustomerModel>();
        public CustomerModel customer = new CustomerModel(1, "Zach", 20);
        // GET: Customer
        public ActionResult Index()
        {
            CM.Add(customer);
            var tuple = new Tuple<CustomerModel, List<CustomerModel>>(customer, CM);
              
            return View("CustomerSelect",tuple);
        }

        [HttpPost]
        public ActionResult OnSelectCustomer(string radioCust)
        {
            customer = new CustomerModel(2, radioCust, 1);
            CM.Add(customer);
            var tuple = new Tuple<CustomerModel, List<CustomerModel>>(customer, CM);

            return View("_CustomerDetails", tuple);
        }

        [HttpPost]
        public string GetMoreInfo(string customerID)
        {
          
            return "Hello There";
        }
    }
}