﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Activity1Part3.Models;
  

namespace Activity1Part3.Services.Data
{
    public class SecurityDAO
    {

        public bool FindByUser(UserModel user)
        {
            //Data Source=(localdb)\MSSQLLocalDB;Integrated Security=True;
            //Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False
            try
            {
                SqlConnectionStringBuilder builder =
                      new SqlConnectionStringBuilder();
                //tried many options for the data source could not get it down.
                builder["Data Source"] = "(localdb)/MSSQLLocalDB";
                builder["integrated Security"] = true;


                string connectionString = builder.ConnectionString;
                string queryString =
                    "SELECT * FROM dbo.Users";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    // Do work here.  
                    SqlCommand command = new SqlCommand(queryString, connection);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            return true;
                           
                        }
                        else
                        {
                            return false;
                            ;
                        }
                        
                    }
                   
                }

            }
            catch 
            {
                return false;
            }
           
           
        }
    }
}