﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Activity1Part3.Services.Data;
using Activity1Part3.Models;

namespace Activity1Part3.Services.Business
{
    public class SecurityService
    {
        public bool Authenticate(UserModel user)
        {
            SecurityDAO service = new SecurityDAO();
            return service.FindByUser(user);
        }
      
    }
}